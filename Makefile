all: bsect
bsect:
	nasm bsect.s -f bin -o bsect
	dd bs=1 skip=51 seek=512 if=apple-green.ppm of=bsect # 51 to skip the header
clean:
	rm bsect
