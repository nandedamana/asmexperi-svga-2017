#!/bin/sh
DEV="sdb"
echo "WARNING: This will affect /dev/$DEV. Press Ctrl+C to quit;\nPress Enter to continue..."
read something
sudo dd bs=446 count=1 conv=notrunc if=bsect of=/dev/$DEV
sudo dd bs=1 count=2 seek=510 skip=510 conv=notrunc if=bsect of=/dev/$DEV
sudo dd bs=512 seek=1 skip=1 count=200 conv=notrunc if=bsect of=/dev/$DEV
