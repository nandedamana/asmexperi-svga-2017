; 8 Mar 2017
; SVGA: Some technical details from: http://www.brokenthorn.com/Resources/OSDevVid2.html

mov si, msg

jmp read_char; TODO FIXME DO NOT SKIP

mov ah, 0x0e ; tty putchar
msg_loop:
mov al, [si]
cmp al, 0
jc read_char
int 0x10
inc si
jmp msg_loop

read_char:
mov ah, 0x00
int 0x16

; set VBE (VESA BIOS Extension) mode no
mov ax, 4f02h
mov bx, 115h; 800x600, 16.8M colours
int 10h

; Read the pic from disc image (bsect)
; Read a sector
mov ah, 02h
mov al, 125h ; no of sectors
mov ch, 0 ; track/cyl. no.
mov cl, 2 ; sector no. We need data after 512 bytes (2th sector for FLOPPY (512 byte sectors))
mov dh, 0 ; head no
; mov dl, 0x80; drive no ; Auto-set by BIOS?

; ES:BX should point to the buffer
mov bx, (0xa000); 0xa000 = svga mem base
mov es, bx
mov bx, 0
int 0x13

mov [di], dword 100100100

; Bank switching
mov ax, 4f05h
mov bx, 0
mov dx, 0

int 10h

jmp $ ; $ => cur addr loop forever

msg db "Press any key to enter the SVGA mode...", 0

times 510 - ($ - $$) db 0; padding
dw 0xaa55 ; boot magic no.

ppm_file_buf db 0
