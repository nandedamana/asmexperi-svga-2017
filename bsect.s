
; 8 Mar 2017
; SVGA: Some technical details from: http://www.brokenthorn.com/Resources/OSDevVid2.html

mov ax, 0x7c0 ; Boot code (this pgm) location in memory
mov ds, ax
mov si, msg

;jmp read_char; TODO FIXME DO NOT SKIP

mov ah, 0x0e ; tty putchar
msg_loop:
mov al, [si]
cmp al, 0
jz read_char
int 0x10
inc si
jmp msg_loop

read_char:
mov ah, 0x00
int 0x16

; set VBE (VESA BIOS Extension) mode no
mov ax, 0x4f02
mov bx, 0x112; 640x480, 16.8M colours
int 10h

; Read the pic from disc image (bsect)
; Read sectors
mov ah, 02h
mov al, 63 ; no of sectors
mov ch, 0 ; track/cyl. no.
mov cl, 2 ; sector no. We need data after 512 bytes (2th sector for FLOPPY (512 byte sectors))
mov dh, 0 ; head no
; mov dl, 0x80; drive no ; Auto-set by BIOS?

; ES:BX should point to the buffer
mov bx, (0x7c0);
mov es, bx
mov bx, ppm_file_buf
int 0x13
jc show_err_read

mov si, 0
mov cx, 40000
draw:
mov bx, 0x7c0;
mov ds, bx

mov dl, [si + ppm_file_buf]

mov bx, 0xa000; 0xa000 = svga mem base
mov ds, bx

mov [si], byte dl
inc si
loop draw

jmp exit

show_err_read:
mov al,03
mov ah,0
int 0x10

mov ax, 0x7c0 ; Boot code (this pgm) location in memory
mov ds, ax
mov si, err_read

mov ah, 0x0e ; tty putchar
msg_loop1:
mov al, [si]
cmp al, 0
jz exit
int 0x10
inc si
jmp msg_loop1

exit:
jmp $

msg db "Press any key to enter the SVGA mode...", 0
err_read db "Error while reading from the disk", 0

times 510 - ($ - $$) db 0; padding
dw 0xaa55 ; boot magic no.

ppm_file_buf db 0
