# Assembly Demo: SVGA and Disk Read

This demo I wrote in 2017 as an attempt to learn how to load an image from the disk
(no filesystem) and display it using x86 Assembly and SVGA BIOS.
